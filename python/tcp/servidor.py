#!/usr/bin/env python

import socket
import threading

from pytz import timezone, all_timezones
from datetime import *


serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

host = socket.gethostname()
porta = 8000
addr = None

serversocket.bind((host, porta))

serversocket.listen(5)

print('Servidor rodando na porta 8000')

def dispara_client(clientsocket):
    while True:
        request = clientsocket.recv(1024)

        if request == 'FIM':
            clientsocket.close
            addr = None

        if request in all_timezones:
            response = str(datetime.now(timezone(request)))
            clientsocket.sendall(response+'\f')
            print(response)
        else:
            clientsocket.sendall("Nao e uma timezone valida!\f")



while True:
    
    clientsocket,addr = serversocket.accept()
    print("Conexao aceita: %s" % str(addr))

    for tmzone in all_timezones:
        clientsocket.sendall(tmzone+'\n')
    clientsocket.sendall('\f')

    client_disparador = threading.Thread(target=dispara_client, args=(clientsocket,))
    client_disparador.start()
    
