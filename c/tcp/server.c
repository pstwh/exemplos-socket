#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>

/*---- Define uma porta ----*/
#define PORTA 8080

int main(int argc, char const *argv[])
{
    int server_fd, novo_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[1024] = {0};
    /*---- Define a mensagem ----*/
    char *ola = "Servidor: Olá";
      
    /*---- Cria o socket, os três argumentos são: ----*/
    /* 1) Domínio de internet 2) Socket de fluxo (TCP) 3) Protocolo padrão (TCP nesse caso) */
    server_fd = socket(AF_INET, SOCK_STREAM, 0);
    setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt));
    
    /*----Configura o struct endereço do servidor ----*/
    /* Address family = Internet */
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    /* Seta o endereço da prova */
    address.sin_port = htons(PORTA);
    
    /*---- Seta o endereço da struct no socket ----*/
    bind(server_fd, (struct sockaddr *)&address, sizeof(address));

    /*---- Ouve no máximo três clientes ----*/
    listen(server_fd, 3);
    
    /*---- Aceita uma chamada e cria um socket para a conexão ----*/
    novo_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen);

    valread = read(new_socket, buffer, 1024);

    printf("%s\n",buffer );

    /*---- Envia a mensagem para o cliente ----*/
    send(novo_socket , ola , strlen(ola) , 0);
    printf("Mensagem enviada!\n");
    return 0;
}