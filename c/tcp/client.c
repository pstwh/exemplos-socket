#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>

/*---- Define uma porta ----*/
#define PORTA 8080
  
int main(int argc, char const *argv[])
{
    struct sockaddr_in address;
    int sock = 0, valread;
    struct sockaddr_in serv_addr;
    /*---- Define a mensagem ----*/
    char *ola = "Cliente: Olá";
    char buffer[1024] = {0};

    /*---- Cria o socket, os três argumentos são: ----*/
    /* 1) Domínio de internet 2) Socket de fluxo (TCP) 3) Protocolo padrão (TCP nesse caso) */
    sock = socket(AF_INET, SOCK_STREAM, 0)

    memset(&serv_addr, '0', sizeof(serv_addr));
  
    /*----Configura o struct endereço do servidor ----*/
    /* Address family = Internet */
    serv_addr.sin_family = AF_INET;
    /* Seta o endereço da prova */
    serv_addr.sin_port = htons(PORTA);
      
    inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr);
    /*---- Conecta no servidor ----*/
    connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr));

    /*---- Envia a mensagem para o servidor ----*/
    send(sock , ola , strlen(ola) , 0);
    printf("Mensagem enviada!\n");
    valread = read( sock , buffer, 1024);
    printf("%s\n",buffer );
    return 0;
}