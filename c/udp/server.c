#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <unistd.h> 
#include <string.h> 

#define LOCAL_SERVER_PORT 1500
#define MAX_MSG 100

int main(int argc, char *argv[]) {
  
  int sd, rc, n, cliLen;
  struct sockaddr_in cliAddr, servAddr;
  char msg[MAX_MSG];

  /*---- Cria o socket, os três argumentos são: ----*/
  /* 1) Domínio de internet 2) Socket de datagrama (UDP) 3) Protocolo padrão (TCP nesse caso) */
  sd=socket(AF_INET, SOCK_DGRAM, 0);

  /* Seta as informações básicas do servidor */
  servAddr.sin_family = AF_INET;
  servAddr.sin_addr.s_addr = htonl(INADDR_ANY);
  servAddr.sin_port = htons(LOCAL_SERVER_PORT);
  /* Atribui struct ao socket */
  rc = bind (sd, (struct sockaddr *) &servAddr,sizeof(servAddr));


  while(1) {
    
    memset(msg,0x0,MAX_MSG);

    cliLen = sizeof(cliAddr);
    n = recvfrom(sd, msg, MAX_MSG, 0, 
		 (struct sockaddr *) &cliAddr, &cliLen);

    printf("%s: de %s:UDP%u : %s \n", 
	   argv[0],inet_ntoa(cliAddr.sin_addr),
	   ntohs(cliAddr.sin_port),msg);
    
  }

return 0;

}
