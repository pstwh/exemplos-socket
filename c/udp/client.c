#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h> 
#include <sys/time.h>  

#define REMOTE_SERVER_PORT 1500
#define MAX_MSG 100


int main(int argc, char *argv[]) {
  
  int sd, rc, i;
  struct sockaddr_in cliAddr, remoteServAddr;
  struct hostent *h;

  /* Chega os argumentos de linha de comando */
  if(argc<3) {
    printf("Modo de uso : %s <server> <data1> ... <dataN> \n", argv[0]);
    exit(1);
  }

  /* Pega o endereço de IP do servidor */
  h = gethostbyname(argv[1]);

  printf("%s: Enviando dados para '%s' (IP : %s) \n", argv[0], h->h_name,	 inet_ntoa(*(struct in_addr *)h->h_addr_list[0]));

  remoteServAddr.sin_family = h->h_addrtype;


  memcpy((char *) &remoteServAddr.sin_addr.s_addr, h->h_addr_list[0], h->h_length);

  /* Porta do servidor */
  remoteServAddr.sin_port = htons(REMOTE_SERVER_PORT);

  /* Cria o socket */
  sd = socket(AF_INET,SOCK_DGRAM,0);
 
  /* Seta alguma porta */
  cliAddr.sin_family = AF_INET;
  cliAddr.sin_addr.s_addr = htonl(INADDR_ANY);
  cliAddr.sin_port = htons(0);
  
  rc = bind(sd, (struct sockaddr *) &cliAddr, sizeof(cliAddr));

  /* Envia dados */
  for(i=2;i<argc;i++) {
    rc = sendto(sd, argv[i], strlen(argv[i])+1, 0, 
		(struct sockaddr *) &remoteServAddr, 
		sizeof(remoteServAddr));
  }
  
  return 1;

}
