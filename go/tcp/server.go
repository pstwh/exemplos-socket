package main

import (
	"bufio"
	"fmt"
	"net"
	"strings"
)

func main() {

	fmt.Println("Iniciando Servidor...")

	// Ouve TCP localmente na porta :8081
	ln, _ := net.Listen("tcp", ":8081")

	// Aceita coneões
	conn, _ := ln.Accept()

	// Roda o loop até um ctrl+c
	for {
		// Vai ouvir a mensagem até uma linha nova
		message, _ := bufio.NewReader(conn).ReadString('\n')
		// Printa a mensagem recebida
		fmt.Print("Messagem Recebida:", string(message))
		// Armazena em maiusculo a mensagem recebida
		newmessage := strings.ToUpper(message)
		// Envia a mensagem recebida
		conn.Write([]byte(newmessage + "\n"))
	}
}
