package main

import (
	"bufio"
	"fmt"
	"net"
	"os"
)

func main() {

	// Conecta
	conn, _ := net.Dial("tcp", "127.0.0.1:8081")
	for {
		// Recebe entrada
		reader := bufio.NewReader(os.Stdin)
		fmt.Print("Escreva o texto para enviar: ")
		text, _ := reader.ReadString('\n')
		// Envia ao socket
		fmt.Fprintf(conn, text+"\n")
		// Ouve a resposta
		message, _ := bufio.NewReader(conn).ReadString('\n')
		fmt.Print("Mensagem do servidor: " + message)
	}
}
